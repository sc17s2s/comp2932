import java.util.*;

// symbols put in symbol table after being declared
public class Symbol {

  public enum SymbolKind {
    stat, field, arg, var
  };

  public SymbolKind kind;
  public String name;
  public int offset;
}
