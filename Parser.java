import java.util.*;
import java.io.*;

// checks if syntax follows the Jack grammar
public class Parser {
  private Lexer lex;
  private SymbolTable[] symbolTables;
  private int currentSymbolTable;
  private String code, tempCode, numCode, symCode;    // for code generation
  private String className;     // store the class name
  private int count;        // number of arguments

  // initialise symbol tables and strings for code generation
  public Parser() {
    symbolTables = new SymbolTable[2];
    symbolTables[0] = new SymbolTable();
    symbolTables[1] = new SymbolTable();
    currentSymbolTable = 0;   //0 is global symbol table
    code = "";
    tempCode = "";
    numCode = "";
    symCode = "";
    className = "";
  }

  // initialise lexer to get tokens
  public void init(String fileName) {
    lex = new Lexer();
    if (!lex.init(fileName)) {
      System.err.println("Lexer not initialised");
    }
    lex.getNextToken();       //returns null but stores token for next time getNextToken is called
    System.out.println("Parser initialised");
  }

  // close file in lexer
  public void close() throws IOException {
    lex.closeFile();
  }

  // useful syntax error message is printed and exits program
  private void error(Token t, String message) {
    System.err.println("Error in line " + t.lineNumber + ", close to " + t.lexeme + ", " + message);
    System.exit(1);
  }

  // prints message for debugging
  private void ok(Token t) {
    // System.out.println(t.lexeme + ": OK");
  }
////////////////////////////////////////////////////////////////////////////////
  public void classDeclar() {
    Token t = lex.getNextToken();

    if (t.lexeme.equals("class")) {
      ok(t);
    } else {
      error(t, "'class' expected");
    }

    t = lex.getNextToken();
    if (t.type == Token.TokenTypes.identifier) {
      ok(t);
      className = t.lexeme;     // store the class for code generation
    } else {
      error(t, "identifier expected");
    }

    t = lex.getNextToken();
    if (t.lexeme.equals("{")) {
      ok(t);
    } else {
      error(t, "'{' expected");
    }

    t = lex.peekNextToken();
    while (!t.lexeme.equals("}")) {
      memberDeclar();
      t = lex.peekNextToken();
    }
    lex.getNextToken(); // consume the }

    // print symbol tables and VM code
    System.out.println("Class scope:");
    symbolTables[0].print();
    System.out.println("----------------");
    System.out.println("Method scope:");
    symbolTables[1].print();
    System.out.println(code);
    System.out.println("----------------\n");
  }
////////////////////////////////////////////////////////////////////////////////
  public void memberDeclar() {
    Token t = lex.peekNextToken();

    if (t.lexeme.equals("static") || t.lexeme.equals("field")) {
      classVarDeclar();
    } else if (t.lexeme.equals("constructor") || t.lexeme.equals("function") || t.lexeme.equals("method")) {
      subroutineDeclar();
    } else {
      error(t, "unknown keyword");
    }
  }
////////////////////////////////////////////////////////////////////////////////
  public void classVarDeclar() {
    Token t = lex.getNextToken();
    int whichKind = 0;      // indicator to differentiate between static and field variables

    if (t.lexeme.equals("static") || t.lexeme.equals("field")) {
      ok(t);
      if (t.lexeme.equals("static")) {
        whichKind = 1;
      } else {
        whichKind = 2;    // field
      }
    } else {
      error(t, "'static' or 'field' expected");
    }

    type();

    t = lex.getNextToken();
    if (t.type == Token.TokenTypes.identifier) {
      ok(t);
      // should not be able to declare more than once
      if (symbolTables[currentSymbolTable].findSymbol(t.lexeme)) {
        error(t, "redeclaration of identifier");
      }
      // add to global symbol table
      if (whichKind == 1) {
        symbolTables[currentSymbolTable].addSymbol(t.lexeme, Symbol.SymbolKind.stat);
      } else if (whichKind == 2) {
        symbolTables[currentSymbolTable].addSymbol(t.lexeme, Symbol.SymbolKind.field);
      }

    } else {
      error(t, "identifier expected");
    }

    // repeat if more than one identifier
    t = lex.peekNextToken();
    while (!t.lexeme.equals(";")) {
      if (t.lexeme.equals(",")) {
        ok(t);
      } else {
        error(t, "',' expected");
      }
      lex.getNextToken();   //consume the ,

      t = lex.getNextToken();
      if (t.type == Token.TokenTypes.identifier) {
        ok(t);
        if (symbolTables[currentSymbolTable].findSymbol(t.lexeme)) {
          error(t, "redeclaration of identifier");
        }
        if (whichKind == 1) {
          symbolTables[currentSymbolTable].addSymbol(t.lexeme, Symbol.SymbolKind.stat);
        } else if (whichKind == 2) {
          symbolTables[currentSymbolTable].addSymbol(t.lexeme, Symbol.SymbolKind.field);
        }

      } else {
        error(t, "identifier expected");
      }

      t = lex.peekNextToken();
    }
    lex.getNextToken(); // consume the ;
  }
////////////////////////////////////////////////////////////////////////////////
  public void type() {
    Token t = lex.getNextToken();

    if (t.lexeme.equals("int") || t.lexeme.equals("char") || t.lexeme.equals("boolean") || t.type == Token.TokenTypes.identifier) {
      ok(t);
    } else {
      error(t, "'int' or 'char' or 'boolean' or identifier expected");
    }
  }
////////////////////////////////////////////////////////////////////////////////
  public void subroutineDeclar() {
    Token t = lex.getNextToken();

    if (t.lexeme.equals("constructor") || t.lexeme.equals("function") || t.lexeme.equals("method")) {
      ok(t);
    } else {
      error(t, "'constructor' or 'function' or 'method' expected");
    }

    t = lex.peekNextToken();
    if (t.lexeme.equals("void")) {
      ok(t);
      lex.getNextToken(); // consume void
    } else {
      type();
    }

    t = lex.getNextToken();
    if (t.type == Token.TokenTypes.identifier) {
      ok(t);
      code = code.concat("\nfunction " + className + "." + t.lexeme);
      currentSymbolTable = 1;   // change to method scope since it is a subroutine

    } else {
      error(t, "identifier expected");
    }

    t = lex.getNextToken();
    if (t.lexeme.equals("(")) {
      ok(t);
    } else {
      error(t, "'(' expected");
    }

    paramList();
    // count the number of arguments
    count = symbolTables[currentSymbolTable].findArg();
    code = code + " " + count;

    t = lex.getNextToken();
    if (t.lexeme.equals(")")) {
      ok(t);
    } else {
      error(t, "')' expected");
    }

    subroutineBody();
  }
////////////////////////////////////////////////////////////////////////////////
  public void paramList() {

    Token t = lex.peekNextToken();
    if (t.lexeme.equals(")")) {
      // no parameters
    } else {
      type();

      t = lex.getNextToken();
      if (t.type == Token.TokenTypes.identifier) {
        ok(t);
        // an argument so add to method scope symbol table
        symbolTables[currentSymbolTable].addSymbol(t.lexeme, Symbol.SymbolKind.arg);

      } else {
        error(t, "identifier expected");
      }

      t = lex.peekNextToken();
      while (!t.lexeme.equals(")")) {
        if (t.lexeme.equals(",")) {
          ok(t);
        } else {
          error(t, "',' expected");
        }
        lex.getNextToken();   //consume the ,

        type();

        t = lex.getNextToken();
        if (t.type == Token.TokenTypes.identifier) {
          ok(t);
          // an argument so add to symbol table
          symbolTables[currentSymbolTable].addSymbol(t.lexeme, Symbol.SymbolKind.arg);

        } else {
          error(t, "identifier expected");
        }

        t = lex.peekNextToken();
      }
    }
  }
////////////////////////////////////////////////////////////////////////////////
  public void subroutineBody() {
    Token t = lex.getNextToken();
    if (t.lexeme.equals("{")) {
      ok(t);
    } else {
      error(t, "'{' expected");
    }

    t = lex.peekNextToken();
    while (!t.lexeme.equals("}")) {
      statement();
      t = lex.peekNextToken();
    }
    lex.getNextToken(); // consume the }
  }
////////////////////////////////////////////////////////////////////////////////
  public void statement() {
    Token t = lex.peekNextToken();
    if (t.lexeme.equals("var")) {
      varDeclarStatement();
    } else if (t.lexeme.equals("let")) {
      letStatement();
    } else if (t.lexeme.equals("if")) {
      ifStatement();
    } else if (t.lexeme.equals("while")) {
      whileStatement();
    } else if (t.lexeme.equals("do")) {
      doStatement();
    } else if (t.lexeme.equals("return")) {
      returnStatement();
    } else {
      error(t, "unknown keyword");
    }
  }
////////////////////////////////////////////////////////////////////////////////
  public void varDeclarStatement() {
    Token t = lex.getNextToken();

    if (t.lexeme.equals("var")) {
      ok(t);
    } else {
      error(t, "'var' expected");
    }

    type();

    t = lex.getNextToken();
    if (t.type == Token.TokenTypes.identifier) {
      ok(t);
      if (symbolTables[currentSymbolTable].findSymbol(t.lexeme)) {
        error(t, "redeclaration of identifier");
      }
      // var so add to method scope symbol table
      symbolTables[currentSymbolTable].addSymbol(t.lexeme, Symbol.SymbolKind.var);

    } else {
      error(t, "identifier expected");
    }

    t = lex.peekNextToken();
    while (!t.lexeme.equals(";")) {
      if (t.lexeme.equals(",")) {
        ok(t);
      } else {
        error(t, "',' expected");
      }
      lex.getNextToken();   //consume the ,

      t = lex.getNextToken();
      if (t.type == Token.TokenTypes.identifier) {
        ok(t);
        if (symbolTables[currentSymbolTable].findSymbol(t.lexeme)) {
          error(t, "redeclaration of identifier");
        }
        symbolTables[currentSymbolTable].addSymbol(t.lexeme, Symbol.SymbolKind.var);

      } else {
        error(t, "identifier expected");
      }

      t = lex.peekNextToken();
    }
    lex.getNextToken(); // consume the ;
  }
////////////////////////////////////////////////////////////////////////////////
  public void letStatement() {
    Token t = lex.getNextToken();

    if (t.lexeme.equals("let")) {
      ok(t);
    } else {
      error(t, "'let' expected");
    }

    t = lex.getNextToken();
    if (t.type == Token.TokenTypes.identifier) {
      ok(t);
      // undeclared identifier should not be assigned to
      if (!symbolTables[currentSymbolTable].findSymbol(t.lexeme)) {
        // check global symbol table if not in current symbol table
        if (!symbolTables[0].findSymbol(t.lexeme)) {
          error(t, "undeclared identifier");
        }
      }
    } else {
      error(t, "identifier expected");
    }

    t = lex.peekNextToken();
    if (t.lexeme.equals("[")) {
      ok(t);
      lex.getNextToken();   // consume the [

      expression();

      t = lex.getNextToken();
      if (t.lexeme.equals("]")) {
        ok(t);
      } else {
        error(t, "']' expected");
      }
    }

    t = lex.getNextToken();
    if (t.lexeme.equals("=")) {
      ok(t);
    } else {
      error(t, "'=' expected");
    }

    expression();
    t = lex.getNextToken();
    if (t.lexeme.equals(";")) {
      ok(t);
    } else {
      error(t, "';' expected");
    }
  }
////////////////////////////////////////////////////////////////////////////////
  public void ifStatement() {
    Token t = lex.getNextToken();

    if (t.lexeme.equals("if")) {
      ok(t);
    } else {
      error(t, "'if' expected");
    }

    t = lex.getNextToken();
    if (t.lexeme.equals("(")) {
      ok(t);
    } else {
      error(t, "'(' expected");
    }

    expression();

    t = lex.getNextToken();
    if (t.lexeme.equals(")")) {
      ok(t);
    } else {
      error(t, "')' expected");
    }

    t = lex.getNextToken();
    if (t.lexeme.equals("{")) {
      ok(t);
    } else {
      error(t, "'{' expected");
    }

    t = lex.peekNextToken();
    while (!t.lexeme.equals("}")) {
      statement();
      t = lex.peekNextToken();
    }
    lex.getNextToken(); // consume the }

    // else part
    t = lex.peekNextToken();
    if (t.lexeme.equals("else")) {
      ok(t);
      lex.getNextToken();   // consume the else

      t = lex.getNextToken();
      if (t.lexeme.equals("{")) {
        ok(t);
      } else {
        error(t, "'{' expected");
      }

      t = lex.peekNextToken();
      while (!t.lexeme.equals("}")) {
        statement();
        t = lex.peekNextToken();
      }
      lex.getNextToken(); // consume the }
    }
  }
////////////////////////////////////////////////////////////////////////////////
  public void whileStatement() {
    Token t = lex.getNextToken();

    if (t.lexeme.equals("while")) {
      ok(t);
    } else {
      error(t, "'while' expected");
    }

    t = lex.getNextToken();
    if (t.lexeme.equals("(")) {
      ok(t);
    } else {
      error(t, "'(' expected");
    }

    expression();

    t = lex.getNextToken();
    if (t.lexeme.equals(")")) {
      ok(t);
    } else {
      error(t, "')' expected");
    }

    t = lex.getNextToken();
    if (t.lexeme.equals("{")) {
      ok(t);
    } else {
      error(t, "'{' expected");
    }

    t = lex.peekNextToken();
    while (!t.lexeme.equals("}")) {
      statement();
      t = lex.peekNextToken();
    }
    lex.getNextToken(); // consume the }
  }
////////////////////////////////////////////////////////////////////////////////
  public void doStatement() {
    Token t = lex.getNextToken();

    if (t.lexeme.equals("do")) {
      ok(t);
    } else {
      error(t, "'do' expected");
    }

    subroutineCall();

    t = lex.getNextToken();
    if (t.lexeme.equals(";")) {
      ok(t);
    } else {
      error(t, "';' expected");
    }
  }
////////////////////////////////////////////////////////////////////////////////
  public void subroutineCall() {
    Token t = lex.getNextToken();
    if (t.type == Token.TokenTypes.identifier) {
      ok(t);
      // call subroutine in VM code
      tempCode = "\ncall " + t.lexeme;
    } else {
      error(t, "identifier expected");
    }

    t = lex.peekNextToken();
    if (t.lexeme.equals(".")) {
      ok(t);
      lex.getNextToken();     // consume the .

      t = lex.getNextToken();
      if (t.type == Token.TokenTypes.identifier) {
        ok(t);
        tempCode = tempCode.concat("." + t.lexeme);
      } else {
        error(t, "identifier expected");
      }
    }

    t = lex.getNextToken();
    if (t.lexeme.equals("(")) {
      ok(t);
    } else {
      error(t, "'(' expected");
    }

    expressionList();

    t = lex.getNextToken();
    if (t.lexeme.equals(")")) {
      ok(t);
    } else {
      error(t, "')' expected");
    }
  }
////////////////////////////////////////////////////////////////////////////////
  public void expressionList() {
    Token t = lex.peekNextToken();
    if (t.lexeme.equals(")")) {
      // no parameters
      code = tempCode.concat(" 0") + code;
    } else {
      expression();
      // join different strings together to form VM code
      code = code + numCode + symCode + tempCode.concat(" 1");

      t = lex.peekNextToken();
      while (!t.lexeme.equals(")")) {
        if (t.lexeme.equals(",")) {
          ok(t);
        } else {
          error(t, "',' expected");
        }
        lex.getNextToken();   //consume the ,

        expression();

        t = lex.peekNextToken();
      }
    }
  }
////////////////////////////////////////////////////////////////////////////////
  public void returnStatement() {
    Token t = lex.getNextToken();

    if (t.lexeme.equals("return")) {
      ok(t);
    } else {
      error(t, "'return' expected");
    }

    t = lex.peekNextToken();
    if (t.lexeme.equals(";")) {
      ok(t);
      // if void so no return expression
      code = code.concat("\npop temp 0\npush constant 0\nreturn");
      lex.getNextToken();   // consume the ;
    } else {
      expression();

      t = lex.getNextToken();
      if (t.lexeme.equals(";")) {
        ok(t);
        code = code.concat("\nreturn");
      } else {
        error(t, "';' expected");
      }
    }
  }
////////////////////////////////////////////////////////////////////////////////
  public void expression() {
    relationalExpression();

    Token t = lex.peekNextToken();
    while (t.lexeme.equals("&") || t.lexeme.equals("|")) {
      ok(t);
      lex.getNextToken();   // consume the & or |

      relationalExpression();
      t = lex.peekNextToken();
    }
  }
////////////////////////////////////////////////////////////////////////////////
  public void relationalExpression() {
    arithmeticExpression();

    Token t = lex.peekNextToken();
    while (t.lexeme.equals("=") || t.lexeme.equals(">") || t.lexeme.equals("<")) {
      ok(t);
      lex.getNextToken();   // consume the = or > or <

      arithmeticExpression();
      t = lex.peekNextToken();
    }
  }
////////////////////////////////////////////////////////////////////////////////
  public void arithmeticExpression() {
    term();

    Token t = lex.peekNextToken();
    while (t.lexeme.equals("+") || t.lexeme.equals("-")) {
      ok(t);
      if (t.lexeme.equals("+")) {
        symCode = "\nadd" + symCode;
      } else {
        symCode = "\nsub" + symCode;
      }
      lex.getNextToken();   // consume the + or -

      term();
      t = lex.peekNextToken();
    }
  }
////////////////////////////////////////////////////////////////////////////////
  public void term() {
    factor();

    Token t = lex.peekNextToken();
    while (t.lexeme.equals("*") || t.lexeme.equals("/")) {
      ok(t);
      if (t.lexeme.equals("*")) {
        symCode = "\ncall Math.multiply 2" + symCode;
      } else {
        symCode = "\ncall Math.divide 2" + symCode;
      }
      lex.getNextToken();   // consume the * or /

      factor();
      t = lex.peekNextToken();
    }
  }
////////////////////////////////////////////////////////////////////////////////
  public void factor() {
    Token t = lex.peekNextToken();
    if (t.lexeme.equals("-") || t.lexeme.equals("~")) {
      ok(t);
      if (t.lexeme.equals("-")) {
        code = "\nneg" + code;
      } else {
        code = "\nnot" + code;
      }
      lex.getNextToken();   // consume the - or ~
    }

    operand();
  }
////////////////////////////////////////////////////////////////////////////////
  public void operand() {
    Token t = lex.getNextToken();
    // integer
    if (t.type == Token.TokenTypes.integer) {
      ok(t);
      numCode = numCode + "\npush constant " + t.lexeme;

    // identifier
    } else if (t.type == Token.TokenTypes.identifier) {
      ok(t);

      t = lex.peekNextToken();
      if (t.lexeme.equals(".")) {
        ok(t);
        lex.getNextToken();   // consume the .

        t = lex.getNextToken();
        if (t.type == Token.TokenTypes.identifier) {
          ok(t);
        } else {
          error(t, "identifier expected");
        }
      }

      t = lex.peekNextToken();
      if (t.lexeme.equals("[")) {
        ok(t);
        lex.getNextToken();   // consume the [

        expression();
        t = lex.getNextToken();
        if (t.lexeme.equals("]")) {
          ok(t);
        } else {
          error(t, "']' expected");
        }
      } else if (t.lexeme.equals("(")) {
        ok(t);
        lex.getNextToken();   // consume the (

        expressionList();
        t = lex.getNextToken();
        if (t.lexeme.equals(")")) {
          ok(t);
        } else {
          error(t, "')' expected");
        }
      }

    // other operands
    } else if (t.type == Token.TokenTypes.stringLiteral) {
      ok(t);
    } else if (t.lexeme.equals("true") || t.lexeme.equals("false") || t.lexeme.equals("null") || t.lexeme.equals("this")) {
      ok(t);
    } else if (t.lexeme.equals("(")) {
      expression();

      t = lex.getNextToken();
      if (t.lexeme.equals(")")) {
        ok(t);
      } else {
        error(t, "')' expected");
      }
    } else {
      error(t, "unknown token");
    }
  }
}
