import java.util.*;

// stores a list of symbols
public class SymbolTable {
  public List<Symbol> table;
  public int count;     // handles the offset number

  public SymbolTable() {
    table = new ArrayList<Symbol>();
    count = 0;
  }

  // creates a symbol and adds to the symbol table
  public void addSymbol(String name, Symbol.SymbolKind kind) {
    Symbol newSymbol = new Symbol();
    newSymbol.name = name;
    newSymbol.kind = kind;
    newSymbol.offset = count++;   //assigns and then increments
    table.add(newSymbol);
  }

  // checks if a certain symbol is already in the symbol table
  public boolean findSymbol(String name) {
    for (Symbol s : table) {
      if (s.name.equals(name)) {
        return true;
      }
    }
    return false;
  }

  // not being used
  // but trying to find the number of arguments
  public int findArg() {
    int a = 0;
    for (Symbol s : table) {
      // if (s.kind.equals(arg)) {
      //   a++;
      // }
    }
    return a;
  }

  // prints symbol table with offset
  public void print() {
    for (Symbol s : table) {
      System.out.println(s.name + ", " + s.offset);
    }
  }
}
