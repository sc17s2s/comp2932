import java.util.*;
import java.io.*;
import java.lang.*;

// reads in characters from file and turns into tokens
public class Lexer {
  private int lineNumber;
  // to help with peeking
  private PushbackReader text;    // can unread - peek at next character in input stream
  private Token currentToken;     // peek at next token without using up
  private Token prevToken;

  // try to read file and initialise variables
  public boolean init(String fileName) {
    //check if file exists
    File f = new File(fileName);
    if (!f.exists()) {
      System.err.println("File does not exist");
      return false;
    }
    //read file
    try {
      FileInputStream file = new FileInputStream(fileName);
      text = new PushbackReader(new InputStreamReader(file));
      System.out.println("Lexer initialised");
    } catch (Exception ex) {
      System.err.println("Error reading file");
    }
    lineNumber = 1;
    currentToken = null;
    prevToken = null;
    return true;
  }

  // close input stream
  public void closeFile() throws IOException {
    text.close();
  }

  // makes a new token (for peek) and returns the previous token
  public Token getNextToken() {
    Token t = new Token();
    int c;    // for reading characters from input stream

    try {
      c = (char)text.read();

      /////** remove if whitespace until first non-whitespace **/////
      while (c != -1 && Character.isWhitespace((char)c)) {
        if (c == '\n') {
          lineNumber++;
        }
        c = (char)text.read();
      }

      /////** skip comments **/////
      // not fully implemented
      if (c == '/') {
        c = (char)text.read();
        // single line comment
        if (c == '/') {
          while (c != '\n') {
            c = (char)text.read();
          }
          c = (char)text.read();    // consume \n
        // multi line comment
        } else if (c == '*') {
          c = (char)text.read();
          while (c != '*') {
            c = (char)text.read();
          }
          c = (char)text.read();
          // if not end of multi line comment
          if (c != '/') {
            while (c != '*') {
              c = (char)text.read();
            }
            c = (char)text.read();
            // if documentation comment
            if (c == '/') {
              c = (char)text.read();
            }
          // end of multi line comment reached
          } else {
            c = (char)text.read();
          }
        // if not a comment
        } else {
          text.unread((int)c);    // unread character after the /
          c = '/';        // continue with / as current character
        }
      }
//----------------------------TOKENS----------------------------//

      /////** EOF **/////
      if (c == -1) {
        System.out.println("eof");
        t.type = Token.TokenTypes.eof;
        t.lineNumber = lineNumber;
        prevToken = currentToken;
        currentToken = t;
        return prevToken;
      }

      /////** string literal **/////
      if (c == '"') {
        t.lexeme += (char)c;
        c = (char)text.read();
        while (c != -1 && c != '"') {
          t.lexeme += (char)c;
          c = (char)text.read();
        }
        t.lexeme += (char)c;
        t.type = Token.TokenTypes.stringLiteral;
        t.lineNumber = lineNumber;
        prevToken = currentToken;
        currentToken = t;
        return prevToken;
      }

      /////** identifiers and keywords **/////
      if (Character.isLetter((char)c) || c == '_') {
        t.lexeme += (char)c;
        c = (char)text.read();
        // can include numbers as well as letters
        while (c != -1 && (Character.isLetter((char)c) || Character.isDigit((char)c))) {
          t.lexeme += (char)c;
          c = (char)text.read();
        }
        text.unread((int)c);    // put last character back in input stream since it's not part of the lexeme

        // list of keywords
        if (t.lexeme.equals("class") || t.lexeme.equals("constructor") || t.lexeme.equals("method")
            || t.lexeme.equals("function") || t.lexeme.equals("int") || t.lexeme.equals("boolean")
            || t.lexeme.equals("char") || t.lexeme.equals("void") || t.lexeme.equals("var")
            || t.lexeme.equals("static") || t.lexeme.equals("field") || t.lexeme.equals("let")
            || t.lexeme.equals("do") || t.lexeme.equals("if") || t.lexeme.equals("else")
            || t.lexeme.equals("while") || t.lexeme.equals("return") || t.lexeme.equals("true")
            || t.lexeme.equals("false") || t.lexeme.equals("null") || t.lexeme.equals("this")) {
          t.type = Token.TokenTypes.keyword;
          t.lineNumber = lineNumber;

        } else {
          // if not a keyword, it's an identifier
          t.type = Token.TokenTypes.identifier;
          t.lineNumber = lineNumber;
        }
        prevToken = currentToken;
        currentToken = t;
        return prevToken;
      }

      /////** integers **/////
      if (Character.isDigit((char)c)) {
        t.lexeme += (char)c;
        c = (char)text.read();
        while (c != -1 && (Character.isDigit((char)c))) {
          t.lexeme += (char)c;
          c = (char)text.read();
        }
        text.unread((int)c);

        t.type = Token.TokenTypes.integer;
        t.lineNumber = lineNumber;
        prevToken = currentToken;
        currentToken = t;
        return prevToken;
      }

      /////** symbol **/////

      // if it is none of the above, it is a symbol
      t.lexeme += (char)c;
      t.type = Token.TokenTypes.symbol;
      t.lineNumber = lineNumber;
      prevToken = currentToken;
      currentToken = t;
      return prevToken;

    } catch (IOException ex) {
      System.err.println("Error reading from input stream");
    }

    return null;
  }

  // returns next token without using it up
  public Token peekNextToken() {
    return currentToken;
  }
}
