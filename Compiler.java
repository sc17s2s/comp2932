import java.util.*;
import java.io.*;

// generates VM code from Jack programs by using parser which uses lexer
public class Compiler {
  public static void main(String[] args) throws IOException {
    if (args.length == 0) {
      System.err.println("No jack program entered");
      System.exit(1);
    } else {
      try {
        /* code for testing lexer
        Lexer lex = new Lexer();

        for (int x = 0; x < args.length; x++) {
          if (lex.init(args[x])) {
            Token[] tokens = new Token[200];
            lex.getNextToken();       // returns null but stores token for next time getNextToken is called

            for (int i = 0; i<200; i++) {
              tokens[i] = lex.getNextToken();
              System.out.println(tokens[i].lexeme);
              System.out.println(tokens[i].type);
              System.out.println("Line: " + tokens[i].lineNumber);
              System.out.println("----------------");
            }
            lex.closeFile();
          }
        }
        */

        for (int i = 0; i < args.length; i++) {
          Parser parse = new Parser();
          parse.init(args[i]);    // initialises parser
          parse.classDeclar();    // starts parser
          parse.close();
        }
      } catch (IOException ex) {
        System.err.println("File not found");
      }
    }
  }
}
