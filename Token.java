import java.util.*;

// tokens produced by lexer and passed onto parser
public class Token {

  public enum TokenTypes {
    keyword, identifier, integer, symbol, eof, stringLiteral
  };

  public String lexeme;
  public TokenTypes type;
  public int lineNumber;

  public Token() {
    lexeme = "";
  }
}
