JACK Compiler

Compile code by inputting in the command line:
javac *.java

Run by inputting:
java Compiler program

Where 'program' is the name of the JACK file if it is in the current directory. To run files in different directories, enter the path name from the current directory.
Multiple JACK files can be entered in the command line to compile them simultaneously.
A separate VM file is not created but the VM code is printed out in the terminal.
